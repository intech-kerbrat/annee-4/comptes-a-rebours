import React from 'react';
import {
	StyleSheet,
	Text,
	View,
} from 'react-native';

import { TimerForm } from './components/TimerForm';
import { TimerList } from './components/TimerList';

export default class App extends React.Component {
	constructor(props) {
		super(props);

		const now = new Date();
		const inTwoMinutes = new Date();
		inTwoMinutes.setMinutes(inTwoMinutes.getMinutes() + 2);

		this.state = {
			timers: [
				{
					label: 'Forum PI',
					start: new Date('2018-09-10'),
					end: new Date('2019-02-01'),
				},
				{
					label: 'Rentrée',
					start: new Date('2018-09-10'),
					end: new Date('2019-03-11'),
				},
				{
					label: 'Dans 2 minutes',
					start: now,
					end: inTwoMinutes,
				}
			],
		};

		this.handleNewTimer = this.handleNewTimer.bind(this);
	}

	handleNewTimer(timer) {
		const timerIndex = this.state.timers.findIndex(existingTimer => existingTimer.label === timer.label);

		if (timerIndex === -1) {
			const timers = Array.from(this.state.timers);
			timers.push(timer);
			this.setState({
				timers: timers,
			});
		}
	}

	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.title}>Comptes à rebours</Text>
				<TimerList style={styles.list} timers={this.state.timers} />
				<TimerForm onNewTimer={this.handleNewTimer} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#fff',
		alignItems: 'stretch',
		justifyContent: 'flex-start',
		padding: 16,
	},
	title: {
		fontSize: 32,
		marginTop: 24,
		marginBottom: 24,
	},
	list: {
		flex: 1,
	}
});
