
import React from 'react';
import {
    Button,
    StyleSheet,
	Text,
	TextInput,
	View,
} from 'react-native';

export class TimerForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			label: '',
			start: '',
			end: '',
		};

		this.handleChangeLabel = this.handleChangeLabel.bind(this);
		this.handleChangeStart = this.handleChangeStart.bind(this);
		this.handleChangeEnd = this.handleChangeEnd.bind(this);
		this.handlePress = this.handlePress.bind(this);
	}

	handleChangeLabel(text) {
		this.setState({ label: text });
	}

	handleChangeStart(text) {
		this.setState({ start: text });
	}

	handleChangeEnd(text) {
		this.setState({ end: text });
	}

	handlePress() {
		const timer = {
			label: this.state.label,
			start: new Date(this.state.start),
			end: new Date(this.state.end),
		};

		this.props.onNewTimer(timer);
		this.setState({
			label: '',
			start: '',
			end: '',
		});

	}

	render() {
		return (
			<View>
				<Text style={styles.title}>Ajouter un compte à rebours</Text>
				<Text>Libellé</Text>
				<TextInput
					style={styles.input}
					placeholder="Entrez le nom du compte à rebours"
					onChangeText={this.handleChangeLabel} />
				<View style={{ flexDirection: 'row' }}>
					<View style={{ flex: 1, marginRight: 4 }}>
						<Text>Date de début</Text>
						<TextInput
							style={styles.input}
							placeholder="AAAA-MM-JJ"
							onChangeText={this.handleChangeStart} />
					</View>
					<View style={{ flex: 1, marginLeft: 4 }}>
						<Text>Date de fin</Text>
						<TextInput
							style={styles.input}
							placeholder="AAAA-MM-JJ"
							onChangeText={this.handleChangeEnd} />
					</View>
				</View>
				<Button
					title="Ajouter"
					onPress={this.handlePress} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	title: {
		fontSize: 24,
		marginTop: 24,
	},
	input: {
		borderWidth: 1,
		borderColor: '#ddd',
		marginBottom: 8,
	},
});
