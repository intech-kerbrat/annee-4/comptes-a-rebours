
import React from 'react';
import {
	FlatList,
	View,
} from 'react-native';

import { TimerListItem } from './TimerListItem';

export class TimerList extends React.Component {
    render() {
        return (
            <View>
                <FlatList
                    data={this.props.timers}
                    keyExtractor={(item) => item.label}
                    renderItem={({ item }) => <TimerListItem timer={item} />}
                    ItemSeparatorComponent={() => <View style={{ height: 1, backgroundColor: '#eee' }} />}
                />
            </View>
        );
    }
}
