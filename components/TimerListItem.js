
import React from 'react';
import {
    StyleSheet,
	Text,
	View,
} from 'react-native';

export class TimerListItem extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			flexProgress: 0,
			flexTotal: 1,
		};

		this.handleInterval = this.handleInterval.bind(this);
		setInterval(this.handleInterval, 1000);
	}

	componentDidMount() {
		this.handleInterval();
	}

	handleInterval() {
		const timer = this.props.timer;
		const progress = Math.min(1, (Date.now() - timer.start) / (timer.end - timer.start));
		const total = 1 - progress;

		this.setState({
			flexProgress: progress,
			flexTotal: total,
		});
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.titleContainer}>
					<Text style={styles.label}>{this.props.timer.label}</Text>
					<Text style={styles.startDate}>({this.props.timer.start.toLocaleString('fr-FR')})</Text>
				</View>
				<View style={styles.progressContainer}>
					<View style={{
						flex: this.state.flexProgress,
						height: 8,
						backgroundColor: 'skyblue'
					}} />
					<View style={{
						flex: this.state.flexTotal,
						height: 8,
						backgroundColor: 'lightgrey'
					}} />
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
        marginTop: 16,
		marginBottom: 16,
		flexDirection: 'column',
		alignItems: 'stretch',
	},
	titleContainer: {
		flexDirection: 'row',
		alignItems: 'baseline',
	},
	label: {
		fontSize: 16,
	},
	startDate: {
		fontSize: 12,
		color: '#999',
		marginLeft: 8,
	},
	progressContainer: {
		flexDirection: 'row',
	},
});
